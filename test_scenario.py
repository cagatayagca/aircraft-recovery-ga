import datetime

import model


class Schedule:
    aircraft = None
    std = None
    sta = None
    departure_airport = None
    arrival_airport = None

    def __init__(self, aircraft, std, sta, departure_airport, arrival_airport):
        self.aircraft = aircraft
        self.std = std
        self.sta = sta
        self.departure_airport = departure_airport
        self.arrival_airport = arrival_airport

    def __str__(self):
        return "\n(DEP_AIRP:" + self.departure_airport + "|ARR_AIRP:" + self.arrival_airport + "|STD:" + str(
            self.std) + "|STA:" + str(self.sta) + "|AIRCRAFT:" + str(self.aircraft) + ")"

    def __repr__(self):
        return str(self)


def create_test_schedule(size_of_the_schedule=10):
    schedule = []
    for idx in range(size_of_the_schedule):
        if idx % 2 == 0:
            dep_airp = 'AAA'
            arr_airp = 'BBB'
        else:
            arr_airp = 'AAA'
            dep_airp = 'BBB'
        current_datetime = datetime.datetime.now()
        hours_1 = datetime.timedelta(hours=1)
        hours_idx = datetime.timedelta(hours=idx)
        schedule.append(Schedule(aircraft=model.Aircraft('A' + str(idx), []), std=current_datetime + hours_idx,
                                 sta=current_datetime + hours_idx + hours_1, departure_airport=dep_airp,
                                 arrival_airport=arr_airp))
    return schedule
