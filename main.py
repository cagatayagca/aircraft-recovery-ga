import random
from random import randrange
import crossovers as cx
import model
import test_scenario

# TODO should or could be read from a config
SCHEDULE_SIZE = 5
POPULATION_SIZE = 5
INITIAL_DISRUPTION_PROBABILITY = 0.01
FLEET = ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10']
DISRUPTED_AIRCRAFT_IDS = list(['A1'])
MAX_NUMBER_OF_DELAYS = 10

#  CX PARAMS
POSITION_BASED_NUMBER_OF_GENES = 3

# SELECTION PARAM
K_FOR_TOURNAMENT = 5

# MUTATION PARAMS
MUTATION_PROB = .1

# COST PARAMS
ATFM_CHARGES_PER_FLIGHT = 1000  # EUROS
DELAYS_IN_MONEY = 3000  # 3 x 100 according to the report
COST_OF_CANCELING_A_FLIGHT = 99999  # TODO correct


def generate_rand_unavailable_slot():  # if needed
    unavailable_slots = []
    possibility = \
        random.choices((1, 0), weights=(INITIAL_DISRUPTION_PROBABILITY, 1 - INITIAL_DISRUPTION_PROBABILITY),
                       k=1)[0]
    if possibility == 1:
        unavailable_slots.append(randrange(0, SCHEDULE_SIZE, 1))
    return unavailable_slots


def init_population(population_size, disrupted_aircraft_list=None):
    if disrupted_aircraft_list is None:
        disrupted_aircraft_list = DISRUPTED_AIRCRAFT_IDS
    population = {}
    for population_idx in range(population_size):
        chromosome = {}
        for schedule_index in range(SCHEDULE_SIZE):
            random_aircraft_id = FLEET[randrange(len(FLEET))]
            while disrupted_aircraft_list.count(random_aircraft_id) > 0:
                random_aircraft_id = FLEET[randrange(len(FLEET))]
            new_gene = model.Gene(randrange(-1, MAX_NUMBER_OF_DELAYS, 1),
                                  model.Aircraft(random_aircraft_id, []))
            chromosome[schedule_index] = new_gene
        population[population_idx] = model.Chromosome(0.0, chromosome)  # TODO fitness calculation
    return population


def tournament_selection(population, num_of_participants=K_FOR_TOURNAMENT):
    population_len = len(population)
    for population_idx in range(population_len):
        tournament_candidates = random.choices(population, k=num_of_participants)
        tournament_candidates = sorted(tournament_candidates, key=lambda chromosome: chromosome.fitness, reverse=False)
    return [tournament_candidates[0], tournament_candidates[1]]


def mutation(population, mutation_probability=MUTATION_PROB):
    population_len = len(population)
    for population_idx in range(population_len):
        for chromosome_idx in range(len(population[population_idx].genes)):
            if random.random() < mutation_probability:
                population[population_idx].genes[chromosome_idx].delay = randrange(-1, MAX_NUMBER_OF_DELAYS, 1)
    return population


def calculate_fitness(population):
    for population_idx in range(len(population)):
        for chromosome_idx in range(len(population[population_idx].genes)):
            cost = ATFM_CHARGES_PER_FLIGHT
            if population[population_idx].genes[chromosome_idx].delay >= 0:
                cost += population[population_idx].genes[chromosome_idx].delay * DELAYS_IN_MONEY
            else:
                cost += COST_OF_CANCELING_A_FLIGHT
            population[population_idx].fitness = cost


def test_runs():
    initial_population = init_population(POPULATION_SIZE, DISRUPTED_AIRCRAFT_IDS)
    print("initial_population=\n", initial_population)
    offspring = cx.position_based([initial_population[0], initial_population[0]], POSITION_BASED_NUMBER_OF_GENES)
    cx.order_based([initial_population[0], initial_population[0]], POSITION_BASED_NUMBER_OF_GENES)
    cx.one_point([initial_population[0], initial_population[0]])
    cx.two_point([initial_population[0], initial_population[0]])
    print(model.Chromosome(0.0, cx.uniform([initial_population[0], initial_population[0]])))  # TODO fitness
    print("SUBSET_BASED =>\n ", model.Chromosome(0.0, cx.subset_based([initial_population[0], initial_population[1]])),
          "---------\n")
    calculate_fitness(initial_population)
    print("TOURNAMENT:\n", tournament_selection(population=initial_population, num_of_participants=K_FOR_TOURNAMENT))
    print("MUTATED: \n", mutation(initial_population))
    print("CUT_AND_SPLICE =>\n ",
          model.Chromosome(0.0, cx.cut_and_splice([initial_population[0], initial_population[1]])),
          "---------\n")

    print("BEFORE SORT:\n", initial_population)
    print("FITNESS CALCULATED:\n",
          sorted(initial_population.items(), key=lambda chromosome: chromosome[1].fitness, reverse=False))

    print("TEST_SCHEDULE:\n", test_scenario.create_test_schedule())


if __name__ == '__main__':
    test_runs()
