class Chromosome:
    """Chromosome structe of the algorithm"""
    fitness = 0.0
    genes = []

    def __init__(self, fitness, genes):
        self.fitness = fitness
        self.genes = genes

    def __str__(self):
        return "[FITNESS:" + str(self.fitness) + " GENES:" + str(self.genes) + "]\n"

    def __repr__(self):
        return str(self)


class Gene:
    """Gene representation of the chromosome.
    Pairs of number of slots to delay the flight in schedule and the aircraft instance"""

    def __init__(self, delay, aircraft):
        self.delay = delay
        self.aircraft = aircraft

    def __str__(self):
        return "[NUMBER_OF_DELAYS:" + str(self.delay) + " AIRCRAFT:" + str(self.aircraft) + "]\n"

    def __repr__(self):
        return str(self)


class Aircraft:
    id = ''
    unavailable_slots = []

    def __init__(self, aircraft_id, available_slots):
        self.id = aircraft_id
        self.unavailable_slots = available_slots

    def __str__(self):
        return "(ID:" + self.id + " NUMBER_OF_UNAVAIL_SLOTS:" + str(len(self.unavailable_slots)) + ")"

    def __repr__(self):
        return str(self)
