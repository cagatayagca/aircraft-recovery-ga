from random import randrange

import numpy


def position_based(parents, number_of_genes):
    offspring = {}
    chromosome_size = len(parents[0].genes)
    for affected_gene in range(number_of_genes):  # choose random genes and push them to the offspring
        parent_1_idx = randrange(0, len(parents[0].genes), 1)
        random_gene_from_parent = parents[1].genes[parent_1_idx]
        offspring[parent_1_idx] = random_gene_from_parent
    for idx in range(chromosome_size):
        if offspring.get(idx) is None:  # fill the rest from the second parent
            offspring[idx] = parents[1].genes[idx]
    return offspring


def order_based(parents, number_of_genes):
    offspring = {}
    chromosome_size = len(parents[0].genes)
    for affected_gene_idx in range(number_of_genes):  # choose random genes and push them to the offspring
        parent_1_idx = randrange(0, len(parents[0].genes), 1)
        random_gene_from_parent = parents[1].genes[parent_1_idx]
        offspring[affected_gene_idx] = random_gene_from_parent
    for idx in range(number_of_genes, chromosome_size):
        offspring[idx] = parents[1].genes[idx]
    return offspring


def one_point(parents):
    offspring = {}
    chromosome_len = len(parents[0].genes)
    cx_point = randrange(1, chromosome_len, 1)  # should not choose the cx point as the first gene.
    for idx_1 in range(cx_point):
        offspring[idx_1] = parents[0].genes[idx_1]
    for idx_2 in range(cx_point, chromosome_len):
        offspring[idx_2] = parents[1].genes[idx_2]
    return offspring


def two_point(parents):
    offspring = {}
    chromosome_len = len(parents[0].genes)
    cx_point_1 = randrange(1, int(chromosome_len / 2), 1)  # should not choose the cx point as the first gene.
    cx_point_2 = randrange(int(chromosome_len / 2), chromosome_len, 1)
    for idx_1 in range(cx_point_1):
        offspring[idx_1] = parents[0].genes[idx_1]
    for idx_2 in range(cx_point_1, cx_point_2):
        offspring[idx_2] = parents[1].genes[idx_2]
    for idx_3 in range(cx_point_2, chromosome_len):
        offspring[idx_3] = parents[0].genes[idx_3]
    return offspring


def uniform(parents, parent_1_probability=.5):
    chromosome_len = len(parents[0].genes)
    offspring = {}
    mask = numpy.random.choice([0, 1], size=chromosome_len, p=[parent_1_probability, 1 - parent_1_probability])
    for idx, parent_idx in enumerate(mask):
        offspring[idx] = parents[parent_idx].genes[idx]
    return offspring


def subset_based(parents):
    chromosome_len = len(parents[0].genes)
    offspring = {}
    subset_start_idx = randrange(0, chromosome_len, 1)
    subset_end_idx = randrange(subset_start_idx, chromosome_len, 1)
    for chromosome_idx in range(chromosome_len):
        if subset_start_idx <= chromosome_idx <= subset_end_idx:
            offspring[chromosome_idx] = parents[0].genes[chromosome_idx]
        else:
            offspring[chromosome_idx] = parents[1].genes[chromosome_idx]
    return offspring


def cut_and_splice(parents):
    chromosome_len = len(parents[0].genes)
    offspring = {}
    cut_point_1 = randrange(1, chromosome_len, 1)
    cut_point_2 = randrange(cut_point_1, chromosome_len, 1)
    for chromosome_idx in range(cut_point_1):
        offspring[chromosome_idx] = parents[0].genes[chromosome_idx]
    for chromosome_idx in range(cut_point_2, chromosome_len):
        offspring[cut_point_1 + 1] = parents[1].genes[chromosome_idx]
    offspring_len = len(offspring)
    size_diff = offspring_len - chromosome_len
    if size_diff > 0:
        offspring = remove_key_from_dict(offspring, randrange(0, offspring_len, 1))
    elif size_diff < 0:
        while size_diff < 0:
            offspring[offspring_len + 1] = parents[1].genes[randrange(0, cut_point_2, 1)]
            size_diff += 1
    return offspring


def remove_key_from_dict(dict_to_remove_from, key):
    temp = dict(dict_to_remove_from)
    del temp[key]
    return temp
